import { Component } from '@angular/core';
import { User } from '../shared/user.model';
import { UserService } from '../shared/userService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hw57';


  constructor(public userService: UserService) {
  }

  addNewUser(user: User) {
    this.userService.users.push(user);
  }
}
