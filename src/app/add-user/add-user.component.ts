import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { User } from '../../shared/user.model';
import { UserService } from '../../shared/userService';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {
  activeCheck = false;

  @Output() newUser = new EventEmitter<User>()
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  @ViewChild('selectInput') selectInput!: ElementRef;
  @ViewChild('check') check!: ElementRef;

  constructor(public userService: UserService) {}

  addNewUser() {
    const name = this.nameInput.nativeElement.value;
    const email = this.emailInput.nativeElement.value;
    const active = this.selectInput.nativeElement.value;
    const check = this.check.nativeElement;

    this.activeCheck = check.checked;

    const user = new User(name, email, active, this.activeCheck);
    this.userService.users.push(user);
  }
}
