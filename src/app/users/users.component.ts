import { Component, Input } from '@angular/core';
import { User } from '../../shared/user.model';
import { UserService } from '../../shared/userService';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {
  @Input() users: User[] = [];
  constructor(public userService: UserService) {}
}
