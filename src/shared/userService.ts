import { User } from './user.model';

export class UserService{
  users: User[] =[
    new User('Jack', 'jack.@gmail.com', 'admin', true),
    new User('Jhon', 'jhon.Dou.@gmail.com', 'admin', true),
    new User('Bob', 'bob.saap.@gmail.com', 'admin', true),
  ];
}
